module.exports = {
  // 表示当前目录即为根目录，Eslint规则将被限制到该目录下
  root: true,
  // env 表示启用eslint检测的环境
  env: {
    // node环境下启动Eslint检测
    node: true
  },
  // Eslint 中基础配置需要继承的配置
  extends: ['plugin:vue/vue3-essential', 'eslint:recommended', '@vue/prettier'],
  // 解析器
  parserOptions: {
    parser: 'babel-eslint'
  },
  // 需要修改的启用规则及其各自的错误级别
  /**
   * 错误级别分为三种
   * 'off' 或者 0 表示关闭规则
   * 'warn' 或者 1 表示开启规则，会进行警告，不会使程序奔溃
   * 'error' 或者 2 开启规则，会使程序崩溃
   */
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    quotes: process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'space-before-function-paren': 'off',
    'no-unused-vars': 'warn',
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': 'off'
  }
}
