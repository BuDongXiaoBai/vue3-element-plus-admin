### Eslint 检测配置

.eslintrc.js
.prettierrc https://www.prettier.cn/docs/index.html
编辑器开启 format on save

### git 提交规范

以 angular 团队为规范为标准，约定式提交 Commitizen

yarn add -g commitizen@4.2.4
yarn add cz-customizable@6.3.0 --save-dev

在 package.json 配置 commitizen
在根目录新建.cz-config.js 自定义提示文件

git cz 代替 git commit

feat：新功能（feature）
fix：修补 bug
docs：文档（documentation）
style： 格式（不影响代码运行的变动）
refactor：重构（即不是新增功能，也不是修改 bug 的代码变动）
test：增加测试
chore：构建过程或辅助工具的变动

### git hook

pre-commit
执行前进行调用，它不接收任何参数，并且在获取提交日志消息并进行提交之前被调用，脚本 git commit 以非零状态退出会导致命令在创建提交之前中止

commit-msg
执行前，可用于将消息规范化为某种项目保准格式。
还可用于在检查消息文件后拒绝提交。

### 最终规范 git 提交实现

我们期望通过 Husky 监测 pre-commit 钩子，在该钩子下执行 npx eslint --ext .js,.vue,.jsx src 指令来去进行相关检测 。  
1.执行 npx husky add .husky/pre-commit "npx eslint --ext .js,.vue,.jsx src"添加 commit 时的 hook。  
（npx eslint --ext .js,.vue src 会在执行到该 hook 时运行） .  
2.该操作会生成对应的文件.husky。
命令执行步骤：
（1）在根目录现在.husky 文件夹.
（2）npx husky add .husky/pre-commit "npx eslint --ext .js,.vue,.jsx src".

### 自动修复代码格式 lint-staged

yarn add lint-staged --save-dev
yarn add husky lint-staged -D
在 package.json 修改配置

### svg 图标

创建 SvgIcon 组件
导入 svg 图标

### schema form

本项目对 el-form 进行了封装，基本满足大部门的业务需求，和传统的 shema form 具有本质区别，如果对 shema form 有兴趣的，可以参考下面的文档
https://vue-json-schema-form.lljj.me/
https://github.com/lljj-x/vue-json-schema-form/tree/master/packages/lib/vue3/vue3-form-element
https://rjsf-team.github.io/react-jsonschema-form/
