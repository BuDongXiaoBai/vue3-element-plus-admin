const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  configureWebpack() {
    return {
      resolve: {
        alias: {
          '@': resolve('src'),
          '*': resolve('')
        }
      },
      plugins: []
    }
  },
  chainWebpack(config) {
    // 设置 svg-sprite-loader
    config.module.rule('svg').exclude.add(resolve('src/icons')).end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  },
  runtimeCompiler: true,
  productionSourceMap: false,
  css: {
    requireModuleExtension: true,
    sourceMap: true,
    loaderOptions: {
      scss: {
        /*sass-loader 8.0语法 */
        prependData:
          '@import "~@/styles/variables.scss"; @import "~@/styles/_mixins.scss";'

        /*sass-loader 9.0写法，感谢github用户 shaonialife*/
        // additionalData(content, loaderContext) {
        //   const { resourcePath, rootContext } = loaderContext
        //   const relativePath = path.relative(rootContext, resourcePath)
        //   if (relativePath.replace(/\\/g, '/') !== 'src/assets/styles/_variables.scss') {
        //     return '@import "~@/styles/assets/_variables.scss";' + content
        //   }
        //   return content
        // }
      }
    }
  }
}
