export default {
  name: 'App',
  setup() {
    return () => <router-view></router-view>
  }
}
