import SelectWidget from './SelectWidget'
import RadioWidget from './RadioWidget'
import CheckboxWidget from './CheckboxWidget'
import UploadWidget from './UploadWidget'
import {
  ElInput,
  ElInputNumber,
  ElDatePicker,
  ElRate,
  ElSlider,
  ElSwitch
} from 'element-plus'

const widgets = {
  input: ElInput,
  number: ElInputNumber,
  select: SelectWidget,
  radio: RadioWidget,
  checkbox: CheckboxWidget,
  date: ElDatePicker,
  rate: ElRate,
  slider: ElSlider,
  switch: ElSwitch,
  upload: UploadWidget
}

export default widgets
