// 导出组件
export { default as AppSearch } from './AppSearch/index.vue'
export { default as Identify } from './Identify/index.vue'

/**
 * 注册全局组件
 * 全局组件必须通过g.js导出
 * 必须有name属性, 即组件名
 * @param {*} app
 */
export const setupGlobalComponent = (app) => {
  const compRequire = require.context('./', true, /\.js$/)

  compRequire.keys().forEach((path) => {
    if (path.endsWith('/g.js')) {
      const comp = compRequire(path).default || compRequire(path)
      app.component(comp.name, comp)
    }
  })
}
