import _ from 'lodash'
// 扁平处理函数
function fillFullPath(routes, parent_path = '') {
  routes.forEach((route) => {
    const { children, path } = route
    route.path = parent_path + path

    if (children && children.length > 0) {
      fillFullPath(children, route.path + '/')
    }
  })
}

export const handleDefaultOpeneds = (routes) => {
  const _routes = _.cloneDeep(routes)
  fillFullPath(_routes)

  const recurs = (_routes) => {
    let result = []
    _routes.forEach((item) => {
      if (item.children && item.children.length > 0) {
        result.push(item.path)
        result = result.concat(...recurs(item.children))
      }
    })
    return result
  }

  return _routes.reduce((prev, item) => {
    return item.children ? prev.concat(...recurs(item.children)) : prev
  }, [])
}
