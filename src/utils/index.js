/**
 * 判断 字符类型
 * @param {*} object
 * @returns
 */
export function typeofFn(object) {
  return Object.prototype.toString.call(object).slice(8, -1).toLowerCase()
}

export function swapItems(value, index1, index2) {
  const arr = [...value]
  arr[index1] = arr.splice(index2, 1, arr[index1])[0]
  return arr
}
