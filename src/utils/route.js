/**
 *
 * @param {*} routes 要递归的路由
 * @param {*} isExitChild true 表示嵌套路由，false 表示不是
 */
export function handleRoutesSort(routes, isExitChild = true) {
  /**
   * 给Array添加一个排序规则的属性
   * @returns
   */
  Array.prototype.routesSort = function () {
    return this.sort((a, b) => {
      if (!a.meta) {
        throw Error(`路由排序，${a.path}缺少元数据`)
      }
      if (!b.meta) {
        throw Error(`路由排序，${b.path}缺少元数据`)
      }
      if (typeof a.meta.sort !== 'number') {
        a.meta.sort = 999999
      }
      if (typeof b.meta.sort !== 'number') {
        b.meta.sort = 999999
      }
      return a.meta.sort - b.meta.sort
    })
  }
  /**
   * 路由排序
   * @param {*} routes 路由列表
   */
  function routesSort(routes) {
    // 同级目录数量至少是2个以上才进行排序，试想一下，一个排序有啥用
    if (routes.length > 1) {
      routes.routesSort()
    }
    // 存在子级路由的列表，进行递归排序
    if (isExitChild) {
      routes.forEach((item) => {
        if (item.children) {
          routesSort(item.children)
        }
      })
    }
  }
  routesSort(routes)
  Array.prototype.routesSort = null
}
