import { reactive } from 'vue'
export default () => {
  const formSchema = reactive({
    status: {
      type: 'default',
      widget: 'switch',
      itemAttrs: {
        label: '状态'
      },
      widgetAttrs: {
        'active-value': '1',
        'inactive-value': '0',
        placeholder: '请输入'
      }
    },
    username: {
      type: 'default',
      widget: 'input',
      description: '用户名不能超过18个字',
      itemAttrs: {
        label: '用户名'
      },
      widgetAttrs: { type: 'string', placeholder: '请输入' }
    },
    avatar: {
      type: 'default',
      widget: 'upload',
      itemAttrs: {
        label: '上传头像'
      },
      widgetAttrs: {
        type: 'image', // file上传文件 image 上传图片
        placeholder: '请输入'
      }
    },

    sex: {
      type: 'default',
      widget: 'radio',
      itemAttrs: {
        label: '性别'
      },
      options: [
        { label: '男', value: 1 },
        { label: '女', value: 0 }
      ]
    },
    age: {
      type: 'default',
      widget: 'number',
      itemAttrs: {
        label: '年龄'
      },
      widgetAttrs: { placeholder: '请输入' }
    },
    hobby: {
      type: 'default',
      widget: 'checkbox',
      itemAttrs: {
        label: '爱好'
      },
      options: [
        { label: '足球', value: 1 },
        { label: '篮球', value: 2 },
        { label: '羽毛球', value: 3 }
      ]
    },
    date: {
      type: 'default',
      widget: 'date',
      itemAttrs: {
        label: '日期'
      },
      widgetAttrs: { placeholder: '请选择', 'value-format': 'YYYY-MM-DD' }
    },
    addressInfo: {
      type: 'object',
      layout: 1,
      gutter: 20,
      childLabelWidth: '0PX',
      itemAttrs: {
        label: '详细地址'
      },

      properties: {
        provice: {
          type: 'default',
          widget: 'select',
          itemAttrs: {
            // label: '省名称'
          },
          widgetAttrs: {
            type: 'string',
            placeholder: '请选择省份',
            onChange(value) {
              console.log(value)
            }
          },
          options: demoApi
        },
        city: {
          type: 'default',
          widget: 'input',
          itemAttrs: {
            // label: '市名称'
          },
          widgetAttrs: { type: 'string', placeholder: '请输入市名称' }
        },
        address: {
          type: 'default',
          widget: 'input',
          itemAttrs: {
            // label: '区名称'
          },
          widgetAttrs: { type: 'string', placeholder: '请输入区名称' }
        },
        addressInfo2: {
          type: 'object',
          layout: 1,
          gutter: 20,
          childLabelWidth: '0PX',
          itemAttrs: {
            label: '详细地址'
          },
          isToggle: true,
          show: false,
          toggle() {
            console.log(123123, this)
            formSchema.addressInfo.properties.addressInfo2.show =
              !formSchema.addressInfo.properties.addressInfo2.show
          },
          properties: {
            provice: {
              type: 'default',
              widget: 'select',
              itemAttrs: {
                // label: '省名称'
              },
              widgetAttrs: {
                type: 'string',
                placeholder: '请选择省份',
                onChange(value) {
                  console.log(value)
                }
              },
              options: demoApi
            },
            city: {
              type: 'default',
              widget: 'input',
              itemAttrs: {
                // label: '市名称'
              },
              widgetAttrs: { type: 'string', placeholder: '请输入市名称' }
            },
            address: {
              type: 'default',
              widget: 'input',
              itemAttrs: {
                // label: '区名称'
              },
              widgetAttrs: { type: 'string', placeholder: '请输入区名称' }
            },
            houseNumber: {
              type: 'array',
              initialState: '',
              itemAttrs: {
                label: '门牌号'
              },
              widgetAttrs: {
                type: 'string',
                placeholder: '请输入门牌号，万一你有多个房子呢'
              },
              onChange(arr) {
                formData.addressInfo2.houseNumber = arr
              }
            }
          }
        }
      }
    },
    todo: {
      type: 'array',
      initialState: '',
      itemAttrs: {
        label: 'todo记录'
      },
      widgetAttrs: { type: 'string', placeholder: '请输入todo' },
      onChange(arr) {
        formData['todo'] = arr
      }
    },
    classes: {
      type: 'array',
      initialState: {
        name: '',
        sex: 0
      },
      itemAttrs: {
        label: '班级信息'
      },
      schema: {
        name: {
          type: 'default',
          widget: 'input',
          widgetAttrs: { type: 'string', placeholder: '请输入用户名' }
        },
        sex: {
          type: 'default',
          widget: 'select',
          widgetAttrs: { type: 'string', placeholder: '请选择性别' },
          options: [
            { label: '男', value: 1 },
            { label: '女', value: 0 }
          ]
        }
      },
      onChange(arr) {
        formData['classes'] = arr
      }
    }
  })
  const formData = reactive({
    status: '1',
    username: '',
    avatar: '',
    sex: 1,
    age: 0,
    date: '',
    hobby: [],
    provice: '',
    city: '',
    address: '',
    addressInfo2: {},
    todo: [''],
    classes: [
      /* { name: '123231', sex: 1 } */
    ]
  })

  const validateAddressInfo = (rule, value, callback) => {
    if (!formData.provice) {
      callback(new Error('请选择省'))
      return
    }
    if (!formData.city) {
      callback(new Error('请选择市'))
      return
    }
    if (!formData.address) {
      callback(new Error('请选择区'))
      return
    }
    callback()
  }
  const formRules = reactive({
    username: [{ required: true, message: '请输入用户名', tigger: 'blur' }],
    addressInfo: [
      { required: true, validator: validateAddressInfo, trigger: 'change' }
    ]
  })

  function demoApi() {
    return new Promise((resolve) => {
      const result = [
        { label: '海南省', value: 1 },
        { label: '广东省', value: 2 }
      ]
      setTimeout(() => {
        resolve(result)
      }, 2000)
    })
  }

  return {
    formSchema,
    formData,
    formRules
  }
}
