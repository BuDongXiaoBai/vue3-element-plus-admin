export const customConfigDefault = {
  // 是否需要 token
  isNeedToken: true,
  // 没有权限的状态码 默认值 401
  notPermissionCode: 401,
  // 是否需要 loading
  isNeedLoading: false,
  // loading 出现的延迟时间
  delay: 300,
  // 是否需要统一处理 error
  isNeedError: true,
  // 是否需要重新请求(请求失败时)
  isRetry: true,
  // 重新请求次数
  connectCount: 3,
  // 是否需要记录错误信息
  isNeedRecordErrorInfo: true,
  // 是否需要缓存
  isNeedCache: false,
  // 重新刷新 token 函数
  refreshToken: undefined,
  // loading 回调函数
  loadingCallback(isShow) {
    console.log('loading', isShow)
  }
}
