const requestMap = new Set()

// 处理重复请求
export const handleRepeat = (requestKey, isAdd = true) => {
  if (!isAdd) {
    return requestMap.delete(requestKey)
  }

  if (requestMap.has(requestKey)) {
    console.log('重复请求已被取消')
    return true
  }
  requestMap.add(requestKey)
}
