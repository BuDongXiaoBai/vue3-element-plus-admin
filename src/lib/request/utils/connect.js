// 记录重连的次数
const connectMap = new Map()

// 处理重新连接
export const handleConnect = ({
  instance, // request实例
  config, // axios 请求配置
  customConfig, // 自定义配置
  requestKey,
  isDelete
}) => {
  if (!customConfig.isRetry) return

  if (isDelete) {
    connectMap.delete(requestKey)
    return
  }

  // 处理重连
  if (!connectMap.has(requestKey)) {
    connectMap.set(requestKey, 1)
  }

  let connectCount = connectMap.get(requestKey)

  if (connectCount <= customConfig.connectCount) {
    connectCount += 1
    connectMap.set(requestKey, connectCount)
    return instance.request(config, customConfig)
  }
  connectMap.delete(requestKey)
}
