/**
 * 创建错误信息
 * @param {*} message   错误信息
 * @param {*} config    axios request config
 * @returns
 */
export const createError = (message, config) => {
  return {
    name: 'custom error',
    message,
    config,
    isAxiosError: false,
    toJSON: () => ({
      message,
      config
    })
  }
}
