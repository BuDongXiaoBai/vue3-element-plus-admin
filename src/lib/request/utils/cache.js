import { createError } from '.'

export class Cache {
  constructor() {
    if (Cache.instance) return Cache.instance

    Cache.instance = this
    this.cache = {}
  }

  get(url) {
    return this.listenError(() => {
      const value = this.cache[url]

      if (value === undefined) {
        throw Error('缓存')
      }

      return value
    })
  }

  set(url, value) {
    return this.listenError(() => {
      return (this.cache[url] = value)
    })
  }

  delete(url) {
    return this.listenError(() => {
      return delete this.cache[url]
    })
  }

  clear() {
    return this.listenError(() => {
      return (this.cache = Object.create(null))
    })
  }

  listenError(cb) {
    return new Promise((resolve, reject) => {
      try {
        resolve(cb.call(this))
      } catch (error) {
        reject(createError('读取缓存数据出错了！', {}))
      }
    })
  }
}
