const errorMap = new WeakMap()

/**
 * 收集错误信息
 * @param {*} instance request实例
 * @param {*} error 错误信息
 */
export const collectError = (instance, error) => {
  const map = errorMap.get(instance) || []
  map.push(error)
  errorMap.set(instance, map)
}

// 获取错误信息
export const getErrorInfo = (instance) => {
  return errorMap.get(instance) || []
}
