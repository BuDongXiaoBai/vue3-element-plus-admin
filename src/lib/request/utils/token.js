/**
 * 处理token
 * @param {Object} config axios request config
 * @param {string} token
 */
export const handleToken = (config, token) => {
  config.headers = { ...(config.headers || {}) }

  config.headers.token = token
}
