let loadingMap = new Map()
let isShowLoading = false

/**
 * 处理 loading
 * @param {Boolean} loading 是否开始
 * @param {String} requestKey
 * @param {number} delay 延迟时间
 * @param {*} loadingCallback 回调函数
 * @returns
 */
export const handleLoading = ({
  loading,
  requestKey,
  delay,
  loadingCallback
}) => {
  let timeId = null

  if (loading) {
    timeId = setTimeout(() => {
      // 没有请求时显示 loading
      if (!isShowLoading) {
        isShowLoading = true
        loadingCallback
          ? loadingCallback(isShowLoading)
          : console.log('start loading')
      }
      clearTimeout(timeId)
    }, delay)

    // 请求之前 添加请求记录与延时器 id
    loadingMap.set(requestKey, timeId)
    return
  }

  // 请求回来之后 删除对应的请求记录
  if (loadingMap.has(requestKey)) {
    const timeId = loadingMap.get(requestKey)
    clearTimeout(timeId)
    loadingMap.delete(requestKey)

    // 没有请求记录之后关闭 loading
    if (isShowLoading && !loadingMap.size) {
      isShowLoading = false
      loadingCallback && typeof loadingCallback === 'function'
        ? loadingCallback(isShowLoading)
        : console.log('end loading')
    }
  }
}
