/**
 * 从字符串头部删除标识符
 * @param str 字符串
 * @param identifier 标识符
 * @returns 处理后字符串
 */
export const removeIdentifierFromStart = (str, identifier) => {
  return str.startsWith(identifier) ? str.substring(1) : str
}

/**
 * 往字符串尾部添加标识符
 * @param str 字符串
 * @param identifier 标识符
 * @returns 处理后字符串
 */
export const addIdentifierToEnd = (str, identifier) => {
  return !str.endsWith(identifier) ? `${str}${identifier}` : str
}

// 转成路径
export const transfromPath = (str, identifier, handleFn) => {
  return handleFn(str, identifier)
}
