import { createApp } from 'vue'
import App from './App.vue'
import { setupRouter } from './router'
import { setupStore } from './store'
import { setupSvgIcon } from './icons'
import '@/styles/index.scss'
import { setupElementComponent } from './plugins/element'
import { setupGlobalComponent } from './components'
import './permission'

const app = createApp(App)
// 设置路由
setupRouter(app)

// 注册vuex状态管理
setupStore(app)

// 引入svg-icon为全局组件，并引入所有的svg图标
setupSvgIcon(app)

// 引入element部分组件
setupElementComponent(app)

// 注册全局组件
setupGlobalComponent(app)

app.mount('#app')
