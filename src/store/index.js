import { createStore } from 'vuex'
import getters from './getters'

/**
 * require.context(文件夹名，是否遍历子目录，匹配后缀名)
 * 返回一个require函数，通过require(path)引入文件
 */
const modulesRequire = require.context('./modules', true, /\.js$/)

const modulesFiles = modulesRequire.keys().reduce((modules, path) => {
  // 设置文件名 './app.js' => 'app'
  const moduleName = path.replace(/^\.\/(.*)\.\w+$/, '$1')

  // 引入文件
  modules[moduleName] = modulesRequire(path).default
  return modules
}, {})

const store = createStore({
  getters: getters,
  modules: modulesFiles
})

export function setupStore(app) {
  app.use(store)
}

export default store
