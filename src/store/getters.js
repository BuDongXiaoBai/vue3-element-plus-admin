import { handleDefaultOpeneds } from '../utils/menu'

const getters = {
  dynamicRoutes: (state) => state.permission.dynamicRoutes, // 动态路由
  token: (state) => state.user.token,
  userInfo: (state) => state.user.userInfo,
  tagsList: (state) => state.tagsView.tagsList, // 路由标签
  cacheViews: (state) => {
    // 缓存的路由
    return state.tagsView.tagsList
      .filter((item) => item.meta && item.meta.keepAlive && item.name)
      .map((item) => item.name)
  },
  defaultOpenMenus: (state) => {
    // 默认展开的ElSubMenu
    return handleDefaultOpeneds(state.permission.dynamicRoutes)
  }
}
export default getters
