/**
 * meta 参数说明
    sort： number        排序, 越小越靠前
    roles: Array<'admin' | 'common'> 设置该路由进入的权限，支持多个权限叠加
    icon: string         svg图标
    title: string        标题
    tags_affix: boolean  是否固定在标签栏
    keepAlive: boolean   是否缓存， 目前还无法缓存到第三级
    alwayShowChildInMenu： boolean  是否一直显示子级菜单，哪怕子菜单只有一个
    hideInMenu: boolean,  是否在菜单栏中隐藏，一般是详情页
    activePath: string    当路由设置了该属性，则会高亮相对应的侧边栏。
 */

import {
  createRouter,
  // createWebHashHistory,
  createWebHistory
} from 'vue-router'
import constantRoutes from './constantModules'
import permissionRoutes from './permissionModules'
import { routesFlat } from '../utils/routes'
export { constantRoutes }

export const asyncRoutes = [...permissionRoutes]
const routes = [...constantRoutes]

// 移除路由组合
let removeRouteGroup = []

// 添加动态路由
export function addRoutes(dynamicRoutes) {
  /**
   * 由于keep-alive 只支持二级路由的缓存，所以应该把二级以上（不包括二级）的路由都提升到二级路由中，
   * routesFlat就是对这个做出处理
   * 这里依然存在一个bug，就是等多级路由的父路由的component存在时，缓存会失效。因为父路由存在component，无法对其进行路由提升
   */
  const routes = routesFlat(dynamicRoutes)

  routes.forEach((route) => {
    const removeRoute = router.addRoute(route)
    removeRouteGroup.push(removeRoute)
  })
}

// 移除动态路由
export function removeRoutes() {
  removeRouteGroup.forEach((removeRoute) => {
    removeRoute()
  })
  removeRouteGroup = []
}

const router = createRouter({
  // history: createWebHashHistory(),
  history: createWebHistory(process.env.BASE_URL),
  routes: routes
})

export function setupRouter(app) {
  app.use(router)
}

export default router
