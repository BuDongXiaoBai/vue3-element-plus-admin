import Layout from '@/layout/index.vue'

const productRouter = [
  {
    path: '/https://www.baidu.com/',
    component: Layout,
    name: 'sadfsf',
    meta: {
      alwayShowChildInMenu: true,
      sort: 9999,
      title: '外链',
      roles: ['admin']
    }
  },
  {
    path: '/product',
    component: Layout,
    redirect: '/product/list',
    name: 'Product',
    meta: {
      alwayShowChildInMenu: true,
      sort: 2,
      title: '产品中心',
      roles: ['admin']
    },
    children: [
      {
        path: 'list',
        name: 'ProductList',
        component: () => import('@/views/products/product-list.vue'),
        redirect: '/product/list/a',
        meta: {
          alwayShowChildInMenu: true,
          icon: 'product',
          title: '产品列表'
        },
        children: [
          {
            path: 'a',
            name: 'ProductListA',
            component: () => import('@/views/products/product-list-a.vue'),
            meta: {
              sort: 2,
              icon: 'product',
              title: '产品列表 A',
              tags_affix: true
            }
          },
          {
            path: 'b',
            name: 'ProductListB',
            meta: {
              sort: 1,
              icon: 'product',
              title: '产品列表 B',
              keepAlive: true
            },
            children: [
              {
                path: 'a',
                name: 'ProductListC',
                component: () => import('@/views/products/product-list-c.vue'),
                meta: {
                  sort: 2,
                  icon: 'product',
                  title: '产品列表 C'
                }
              }
            ]
          }
        ]
      },
      {
        path: 'detail',
        name: 'ProductDetail',
        component: () => import('@/views/products/product-detail.vue'),
        meta: {
          title: '产品详情',
          keepAlive: true,
          hideInMenu: true,
          activePath: '/product/list/a'
        }
      }
    ]
  }
]

export default productRouter
