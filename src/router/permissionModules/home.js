import Layout from '@/layout/index.vue'

const homeRouter = [
  {
    path: '/home',
    component: Layout,
    redirect: '/home/index',
    meta: {
      sort: 1,
      title: '首页'
    },
    children: [
      {
        path: 'index',
        name: 'Home',
        component: () => import('@/views/home/index.vue'),
        meta: {
          sort: 0,
          icon: 'home',
          title: '首页',
          tags_affix: true
        }
      }
    ]
  }
]

export default homeRouter
