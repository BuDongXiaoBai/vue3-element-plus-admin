import Layout from '@/layout/index.vue'

const errorRouter = [
  {
    path: '/error',
    component: Layout,
    redirect: 'noRedirect',
    name: 'Error',
    meta: { title: '错误页', hideInMenu: true, sort: 9999998 },
    children: [
      {
        path: '401',
        name: 'Error401',
        component: () => import('@/views/error/401'),
        meta: { title: '401' }
      },
      {
        path: '404',
        name: 'Error404',
        component: () => import('@/views/error/404'),
        meta: { title: '404' }
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/error/404',
    meta: {
      sort: 9999999,
      hideInMenu: true
    }
  }
]

export default errorRouter
