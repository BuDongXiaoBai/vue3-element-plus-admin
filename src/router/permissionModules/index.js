/**
 * 引入所有的异步组件
 */
const requirePermissionRoutes = () => {
  const modulesRequire = require.context('./', true, /\.js$/)
  let permissionModules = []
  modulesRequire.keys().forEach((key) => {
    if (key === './index.js') return
    permissionModules.push(...modulesRequire(key).default)
  })
  return permissionModules
}

const permissionRoutes = requirePermissionRoutes()

export default permissionRoutes
