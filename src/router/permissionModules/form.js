import Layout from '@/layout/index.vue'

const TableRouter = [
  {
    path: '/form',
    component: Layout,
    redirect: '/form/index',
    meta: {
      icon: 'form',
      sort: 20,
      title: '表单'
    },
    children: [
      {
        path: 'index',
        name: 'Form',
        component: () => import('@/views/form/index.vue'),
        meta: {
          icon: 'form',
          title: '表单'
        }
      }
    ]
  }
]

export default TableRouter
