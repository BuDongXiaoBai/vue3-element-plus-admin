import Layout from '@/layout/index.vue'

const productRouter = [
  {
    path: '/menu',
    component: Layout,
    redirect: '/menu/menu-1',
    name: 'Menu',
    meta: {
      alwayShowChildInMenu: true,
      sort: 10,
      title: '菜单'
    },
    children: [
      {
        path: 'menu-1',
        name: 'Menu1',
        redirect: '/menu/menu-1/menu-1-1',
        meta: {
          alwayShowChildInMenu: true,
          icon: 'product',
          title: '菜单-1'
        },
        children: [
          {
            path: 'menu-1-1',
            name: 'Menu11',
            redirect: '/menu/menu-1/menu-1-1/menu-1-1-1',
            meta: {
              alwayShowChildInMenu: true,
              icon: 'product',
              title: '菜单-1-1'
            },
            children: [
              {
                path: 'menu-1-1-1',
                name: 'Menu111',
                component: () => import('@/views/menu/menu.vue'),
                meta: {
                  alwayShowChildInMenu: true,
                  icon: 'product',
                  title: '菜单-1-1-1',
                  keepAlive: true // 缓存，这个必须设置
                }
              }
            ]
          }
        ]
      }
    ]
  }
]

export default productRouter
