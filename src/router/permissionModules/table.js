import Layout from '@/layout/index.vue'

const TableRouter = [
  {
    path: '/table',
    component: Layout,
    redirect: '/table/index',
    meta: {
      sort: 10,
      title: '表格',
      icon: 'table'
    },
    children: [
      {
        path: 'index',
        name: 'Table',
        component: () => import('@/views/table/index.vue'),
        meta: {
          icon: 'table',
          title: '表格'
        }
      }
    ]
  }
]

export default TableRouter
