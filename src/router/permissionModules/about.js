import Layout from '@/layout/index.vue'

const aboutRouter = [
  {
    path: '/about',
    component: Layout,
    redirect: '/about/index',
    meta: {
      alwayShowChildInMenu: true,
      sort: 10,
      title: '关于我们'
      // roles: ['common']
    },
    children: [
      {
        path: 'index',
        name: 'About',
        component: () => import('@/views/about/index.vue'),
        meta: {
          icon: 'about',
          title: '关于我们'
        }
      }
    ]
  }
]

export default aboutRouter
