import 'element-plus/dist/index.css'

import {
  // 布局相关组件
  ElContainer,
  ElAside,
  ElHeader,
  ElMain,
  ElRow,
  ElCol,
  ElCard,
  ElDivider,
  ElSpace,
  // 功能相关组件
  ElLink,
  ElTag,
  ElButton,
  ElSelect,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElIcon,
  ElDropdown,
  ElDropdownMenu,
  ElDropdownItem,
  ElPopover,
  ElForm,
  ElFormItem,
  ElInput,
  ElDialog,
  ElScrollbar,
  ElMenu,
  ElMenuItem,
  ElSubMenu,
  ElPageHeader,
  // 反馈组件
  ElMessage,
  ElMessageBox,
  ElNotification,
  ElLoading
  // ElTooltip,
} from 'element-plus'

const plugins = [ElLoading, ElMessage, ElMessageBox, ElNotification]

const components = [
  ElContainer,
  ElAside,
  ElHeader,
  ElMain,
  ElRow,
  ElCol,
  ElSpace,
  ElTag,
  ElLink,
  ElCard,
  ElDivider,
  ElButton,
  ElSelect,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElIcon,
  ElDropdown,
  ElDropdownMenu,
  ElDropdownItem,
  ElPopover,
  ElForm,
  ElFormItem,
  ElInput,
  ElDialog,
  ElScrollbar,
  ElMenu,
  ElMenuItem,
  ElSubMenu,
  ElPageHeader
]

export function setupElementComponent(app) {
  app.config.globalProperties.$ELEMENT = {
    // options
    size: 'small',
    zIndex: 3000
  }

  for (const component of components) {
    app.component(component.name, component)
  }

  plugins.forEach((plugin) => {
    app.use(plugin)
  })
  /**
   * 插件使用
   * ctx.appContext.config.globalProperties.$loading()
   */
}
