import { computed } from 'vue'
import variables from '@/styles/_variables.scss'
export default (isShowSlideBar) => {
  const topSlideBarWidth = computed(() => variables.topSlideBarWidth)
  const slideBarWidth = computed(() => variables.slideBarWidth)
  const sideWidth = computed(() => {
    let width = 0
    if (isShowSlideBar.value) {
      width = parseInt(topSlideBarWidth.value) + parseInt(slideBarWidth.value)
    } else {
      width = parseInt(topSlideBarWidth.value)
    }
    return width
  })
  const topSlideBarStyle = computed(() => {
    return {
      width: topSlideBarWidth.value,
      flex: `0 0 ${topSlideBarWidth.value}`
    }
  })
  const slideBarStyle = computed(() => {
    return {
      width: slideBarWidth.value,
      flex: `0 0 ${slideBarWidth.value}`
    }
  })
  return {
    sideWidth,
    topSlideBarStyle,
    slideBarStyle
  }
}
