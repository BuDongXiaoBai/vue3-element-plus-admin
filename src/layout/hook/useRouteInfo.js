import { useRoute /* useRouter */ } from 'vue-router'
import { fetchTopRoutePath } from '@/utils/util'
import { computed } from 'vue'

export default (menus) => {
  const route = useRoute()
  // const router = useRouter()
  // 当前跳转到的路径
  const active_path = computed(() => route.path)

  // 第一级路由的路径
  const active_top_path = computed(() => '/' + fetchTopRoutePath(route.path))

  // 子级路由列表
  const active_route = computed(() => {
    return menus.find((item) => item.path === active_top_path.value) || []
  })

  return {
    active_path,
    active_top_path,
    active_route
  }
}
