import { renderMenuContent } from './render'
import './style.scss'
import { ElScrollbar, ElMenu, ElDivider } from 'element-plus'
// import { useStore } from 'vuex'
import { watch, nextTick, ref } from 'vue'

export default {
  name: 'SlideBar',
  props: {
    route: Object,
    activePath: String
  },
  components: {
    ElScrollbar,
    ElMenu,
    ElDivider
  },
  setup(props) {
    // const menus = computed(() => props.route.children || [])
    // const title = computed(() => props.route.meta?.title || '')
    // const path = computed(() => props.route.path)
    // const store = useStore()
    const menuRef = ref()

    // 当路由跳转时，展开对应的路由菜单
    const handleMenuOpen = (path) => {
      const arr = path.slice(1).split('/')
      if (arr.length >= 3) {
        let length = 1
        let path = '/' + arr[0]
        while (length < arr.length - 1) {
          path += '/' + arr[length]
          menuRef.value?.open(path)
          length++
        }
      }
    }

    watch(
      () => props.activePath,
      async (newVal) => {
        await nextTick()
        handleMenuOpen(newVal)
      }
    )
    return () => {
      const menus = props.route.children
      const title = props.route.meta?.title || ''
      // const defaultOpeneds = store.getters.defaultOpenMenus || []
      return (
        <div class={'app-slide-bar'}>
          <el-scrollbar wrap-class="scrollbar-wrapper">
            <router-link to="/" className="title">
              <h1>欢迎来到Admin</h1>
              <el-divider content-position="center" class="divider">
                {title}
              </el-divider>
            </router-link>
            <div class="menus-wrap">
              <el-menu
                ref={(el) => (menuRef.value = el)}
                style={{ border: 'none' }}
                unique-opened={false}
                default-active={props.activePath}
                // default-openeds={defaultOpeneds}
              >
                {menus.map((item) => renderMenuContent()(item, props.route))}
              </el-menu>
            </div>
          </el-scrollbar>
        </div>
      )
    }
  }
}
