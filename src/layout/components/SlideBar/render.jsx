import _ from 'path'
import { ElSubMenu, ElMenuItem } from 'element-plus'
import { useRoute, useRouter } from 'vue-router'
import { isExternal } from '../../../utils/validate'
/**
 * 如果子级显示的数量是1，那就不显示子菜单，子菜单当作父级菜单显示
 * 如果子级显示的数量大于1，那就显示子菜单
 * 什么叫子级显示数量，因为子级存在hideInMenu情况，显示数量不一定等于子级数量
 * @param AppRouteRecordRaw
 * @returns components组件 number子级数量
 */
function showChildren({ children }) {
  if (children) {
    const showChildren = children.filter((item) => {
      return !(item.meta && item.meta.hideInMenu)
    })
    return {
      components: showChildren,
      number: showChildren.length
    }
  }
  return {
    number: 0,
    components: null
  }
}

/**
 * 菜单项渲染
 * @param path 路由路径
 * @param title 路由标题
 * @returns
 */
function renderLink({ path, title, icon }) {
  const router = useRouter()
  const route = useRoute()
  const jump = (path) => {
    if (isExternal(path)) {
      window.location.href = path
    } else {
      router.push(path)
    }
  }
  return (
    <ElMenuItem
      index={path}
      key={path}
      onClick={() => jump(path)}
      class={
        route.path === path || route.meta.activePath === path
          ? 'is-active-menu'
          : ''
      }
    >
      <span class="route-icon">
        <svg-icon icon={icon}></svg-icon>
      </span>
      <span class="route-name">{title}</span>
    </ElMenuItem>
  )
}

/**
 * 生成菜单内容
 * @returns
 */
export function renderMenuContent() {
  /**
   * @param child 子级路由
   * @param parent 父级路由
   */
  return function recursive(child, parent) {
    const { meta, children, path } = child

    // 是否是外链
    if (isExternal(path)) {
      return renderLink({ path: path, ...meta })
    }

    const { components, number: showChildrenNumber } = showChildren(
      child,
      alwayShowChildInMenu
    )
    let alwayShowChildInMenu = false
    let _path = path
    if (parent) {
      const { path: parent_path } = parent
      alwayShowChildInMenu = parent.meta?.alwayShowChildInMenu
      _path = _.resolve(parent_path, path)
      child.path = _path
    } else {
      _path = '/' + _path
    }

    // 如果 hideInMenu 为true，则什么都不显示
    if (meta?.hideInMenu) {
      return null
    }

    if (
      showChildrenNumber > 1 ||
      (showChildrenNumber === 1 && alwayShowChildInMenu)
    ) {
      return (
        <ElSubMenu
          index={_path}
          key={_path}
          v-slots={{
            title: () => <span>{meta?.title}</span>
          }}
        >
          {children.map((item) => {
            return recursive(item, child)
          })}
        </ElSubMenu>
      )
    }

    // 如果显示为一，就显示 renderLink
    if (showChildrenNumber === 1) {
      const { meta, path } = components[0]
      _path = _.resolve(child.path, path)
      return renderLink({ path: _path, ...meta })
    }

    return renderLink({ path: _path, ...meta })
  }
}
