import { ref } from 'vue'
export default () => {
  const showSearch = ref(false)

  // 点击搜索
  const openSearch = () => {
    showSearch.value = true
  }
  const closeSearch = () => {
    showSearch.value = false
  }
  return {
    showSearch,
    openSearch,
    closeSearch
  }
}
