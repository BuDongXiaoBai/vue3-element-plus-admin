import { ref, onMounted, onUnmounted } from 'vue'
import screenfull from 'screenfull'
import { ElMessage } from 'element-plus'
export default () => {
  const isFullscreen = ref(false)
  const onScreenfull = () => {
    if (!screenfull.isEnabled) {
      ElMessage({
        message: '你的浏览器支持该功能',
        type: 'warning'
      })
      return false
    }
    screenfull.toggle()
  }
  const change = () => {
    isFullscreen.value = screenfull.isFullscreen
  }
  const init = () => {
    if (screenfull.isEnabled) {
      screenfull.on('change', change)
    }
  }
  onMounted(() => {
    init()
  })
  onUnmounted(() => {
    if (screenfull.isEnabled) {
      screenfull.off('change', change)
    }
  })
  return {
    isFullscreen,
    onScreenfull
  }
}
