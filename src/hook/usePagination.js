import { reactive } from 'vue'

/**
 * params Object | Function
 * callback undefined | Function
 */
export default (params, callback) => {
  let pageSize = 10
  let total = 0
  let currentPage = 1

  // 回调函数
  let cb = () => {}
  if (params && typeof params === 'object') {
    pageSize = params.pageSize || pageSize
    total = params.total || total
    currentPage = params.currentPage || currentPage

    cb = typeof callback === 'function' ? callback : cb
  } else if (typeof params === 'function') {
    cb = params
  }

  const handleCurrentChange = (value) => {
    pageOptions.currentPage = value
    cb(value)
  }

  const pageOptions = reactive({
    pageSize,
    total,
    currentPage
  })

  return {
    pageOptions,
    handleCurrentChange
  }
}
