import { onBeforeMount, onBeforeUnmount } from 'vue'

export default () => {
  const random_id = 'sdfsfskfhskdf'
  const element_id = '#' + random_id
  onBeforeMount(() => {
    const el = document.createElement('div')
    el.id = random_id
    const body = document.body
    body.appendChild(el)
  })

  onBeforeUnmount(() => {
    const el = document.querySelector(element_id)
    el.parentNode.removeChild(el)
  })

  return {
    element_id
  }
}
