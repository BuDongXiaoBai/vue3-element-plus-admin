import { ElMessageBox } from 'element-plus'

export const useConfirm = ({ title, ok, cancel }) => {
  ElMessageBox.confirm('是否确定删除该项' || title, '提示', {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning'
  })
    .then(() => {
      ok && typeof ok === 'function' && ok()
    })
    .catch(() => {
      cancel && typeof cancel === 'function' && cancel()
    })
}
