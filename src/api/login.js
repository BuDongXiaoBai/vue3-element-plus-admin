import request from '../lib/request'

export const login = (customConfig) =>
  request.get({ url: '/login' }, customConfig)
