import SvgIcon from '../components/SvgIcon'
/**
 * require.context 返回一个key -> require函数
 */
const svgRequire = require.context('./svg', false, /\.svg$/)

svgRequire.keys().forEach((svgIcon) => {
  svgRequire(svgIcon)
})
// const req = require.context('./svg', false, /\.svg$/),
//   requireAll = (requireContext) => {
//     return requireContext.keys().map(requireContext)
//   }
// requireAll(req)

export const setupSvgIcon = (app) => {
  app.component(SvgIcon.name, SvgIcon)
}
